<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<div>
    <br/>
    <c:url value="/login" var="loginUrl"/>
    <form action="${loginUrl}" method="post">
        <table>
            <c:if test="${param.error != null}">
                <p>
                    Invalid username and password.
                </p>
            </c:if>
            <c:if test="${param.logout != null}">
                <p>
                    You have been logged out.
                </p>
            </c:if>
            <tr>
                <td><label for="username">Login: </label></td>
                <td><input type="text" name="username" id="username" value=""
                           placeholder="Input your username"></td>
            </tr>
            <tr>
                <td><label for="password">Password:</label></td>
                <td>
                    <input type="password" name="password" id="password" value=""
                           placeholder="Input your password">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <input type="submit" value="Submit">
                </td>
            </tr>
            <tr/>
            <tr>
                <td>or <a href="registration">Register</a></td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>
