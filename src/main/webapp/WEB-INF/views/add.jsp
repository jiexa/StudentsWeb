<%--
  Created by IntelliJ IDEA.
  User: jiexa
  Date: 23.02.17
  Time: 22:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<html>
<head>
    <title>Adding Student</title>
</head>
<body>
<h1>Adding student</h1>
<form action="add" method="post">
    <table>
        <tr>
            <th>Field name</th>
            <th>Value</th>
        </tr>
        <tr>
            <td>Name</td>
            <td><input type="text" name="name" id="name"/>
        </tr>
        <tr>
            <td>Birth Day</td>
            <td><input type="date" name="birthDate" id="birthDate"/>
        </tr>
        <tr>
        <tr>
            <td>Sex</td>
            <td>
                <select name="isMale" id="isMale">
                    <option value="true">М</option>
                    <option value="false">Ж</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Group Id</td>
            <td><input type="text" name="groupId" id="groupId"/>
        </tr>
    </table>
    <input type="submit" value="Create">
</form>
</body>
</html>
