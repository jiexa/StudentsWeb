<%--
  Created by IntelliJ IDEA.
  User: jiexa
  Date: 23.02.17
  Time: 22:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<html>
<head>
    <title>Adding Lecture</title>
</head>
<body>
<h1>Adding Lecture</h1>
<form action="lecture_add" method="post">
    <table>
        <tr>
            <th>Field name</th>
            <th>Value</th>
        </tr>
        <tr>
            <td>Lecture</td>
            <td><input type="text" name="addName"/>
        </tr>
        <tr>
            <td>Subject</td>
            <td><input type="text" name="addSubject"/>
        </tr>
        <tr>
            <td>Content</td>
            <td><input type="text" name="addText"/>
        </tr>
        <tr>
            <td>Group Id</td>
            <td><input type="number" name="addGroup"/>
        </tr>
        <tr>
            <td>Date & Time</td>
            <td><input type="datetime" name="addDateTime"/>
        </tr>
        <tr>
    </table>
    <input type="submit" value="Create">
</form>
</body>
</html>
