<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"  session="false" %>
<html>
<head>
    <title>List</title>
</head>
<body>

<h1>List of Lectures</h1>
<table width="70%" border="1px solid black">
    <tr>
        <th width="5%">№</th>
        <th width="10%">Лекция</th>
        <th width="10%">Предмет</th>
        <th width="20%">Содержание</th>
        <th width="5%">Номер группы</th>
        <th width="10%">Время и дата проведения</th>
        <th width="20%">Действие</th>
    </tr>
    <c:forEach items="${lectures}" var="l">
        <tr>
            <td>${l.id}</td>
            <td>${l.name}</td>
            <td>${l.subject}</td>
            <td>${l.text}</td>
            <td>${l.groupId}</td>
            <td>${l.dateTime.getTime()}</td>
            <td align="center">
                <form name="actionForm" action="lecture_list" method="post">
                    <select name="action">
                        <c:forEach var="a" items="${actions}">
                            <option value="${a.value}">${a.key}</option>
                        </c:forEach>
                    </select>
                    <input type="hidden" name="selectedLecture" value="${l}">
                    <input type="submit" value="Do" />
                </form>
            </td>
        </tr>
    </c:forEach>
</table>
<a href="lecture_add">Add lecture</a>
</body>
</html>
