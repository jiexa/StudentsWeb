<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<html>
<head>
    <title>Editing Lecture</title>
</head>
<body>
<h1>Editing Lecture</h1>

<%--<c:out value="${editingLecture.name}"/>--%>
<form action="lecture_edit" method="post">
    <table>
        <tr>
            <th>Field name</th>
            <th>Current values</th>
            <th>Your values</th>
        </tr>
        <tr>
            <td>Name</td>
            <td>${editingLecture.name}</td>
            <td><input type="text" name="editName" value="${editingLecture.name}"/>
        </tr>
        <tr>
            <td>Содержание</td>
            <td>${editingLecture.text}</td>
            <td><input type="text" name="editText" value="${editingLecture.text}"/>
        </tr>
    </table>
    <input type="submit" value="Update">
</form>
</body>
</html>
