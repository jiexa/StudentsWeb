<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"  session="false" %>
<html>
<head>
    <title>List</title>
</head>
<body>

<h1>List of Students</h1>
<table width="550px" border="1px solid black">
    <tr>
        <th width="30%">Имя</th>
        <th width="25%">Дата рождения</th>
        <th width="15%">Группа</th>
        <th width="30%">Действие</th>
    </tr>
    <c:forEach items="${students}" var="s">
        <tr>
            <td>${s.name}</td>
            <td>${s.birthDate}</td>
            <td>${s.groupId}</td>
            <td align="center">
                <form name="actionForm" action="list" method="post">
                    <select name="action" id="action">
                        <c:forEach var="a" items="${actions}">
                            <option value="${a.value}">${a.key}</option>
                        </c:forEach>
                    </select>
                    <input type="hidden" name="selectedStudent" value="${s}">
                    <input type="submit" value="Do" />
                </form>
            </td>
        </tr>
    </c:forEach>
</table>

<a href="add">Add student</a>
<br/>
<a href="lecture_list">See list of lectures</a>
<br/>
<a href="logout">Logout</a>
</body>
</html>
