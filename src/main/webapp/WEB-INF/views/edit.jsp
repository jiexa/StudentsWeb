<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<html>
<head>
    <title>Editing Student</title>
</head>
<body>
<h1>Editing Student</h1>

<%--<c:out value="${editingStudent.name}"/>--%>
<form action="edit" method="post">
    <table>
        <tr>
            <th>Field name</th>
            <th>Current values</th>
            <th>Your values</th>
        </tr>
        <tr>
            <td>Name</td>
            <td>${editingStudent.name}</td>
            <td><input type="text" name="name" value="${editingStudent.name}"/>
        </tr>
        <tr>
            <td>Birth Day</td>
            <td>${editingStudent.birthDate}</td>
            <td><input type="date" name="birthDate" value="${editingStudent.birthDate}"/>
        </tr>
        <tr>
            <td>Group Id</td>
            <td>${editingStudent.groupId}</td>
            <td><input type="text" name="groupId" value="${editingStudent.groupId}"/>
        </tr>
    </table>
    <input type="submit" value="Update">
</form>
</body>
</html>
