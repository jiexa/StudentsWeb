package com.malyshev.config;

import com.malyshev.controllers.listeners.SessionListener;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Created by A.Malyshev on 06.03.17.
 */
public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
      
      @Override
      protected Class< ? >[] getRootConfigClasses( ) {
            return new Class< ? >[]{WebConfig.class};
      }
      
      @Override
      protected Class< ? >[] getServletConfigClasses( ) {
            return null;
      }
      
      @Override
      protected String[] getServletMappings( ) {
            return new String[]{"/"};
      }

      @Override
      public void onStartup( ServletContext servletContext ) throws ServletException {
            super.onStartup( servletContext );
            servletContext.addListener( SessionListener.class );
      }
}
