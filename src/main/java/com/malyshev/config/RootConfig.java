package com.malyshev.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by A.Malyshev on 06.03.17.
 */
@Configuration
@ComponentScan(basePackages={"com.malyshev"},
     excludeFilters={
          @Filter(type= FilterType.ANNOTATION, value=EnableWebMvc.class)
     })
public class RootConfig {
}