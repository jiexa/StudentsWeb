package com.malyshev.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * Created by A.Malyshev on 01.03.17.
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.malyshev.controllers", "com.malyshev.services", "com.malyshev.models.dao"})
@Import({ WebSecurityConfig.class })
public class WebConfig extends WebMvcConfigurerAdapter {
      
      @Bean(name = "dataSource")
      public DriverManagerDataSource dataSource() {
            DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
            driverManagerDataSource.setDriverClassName("org.postgresql.Driver");
            driverManagerDataSource.setUrl("jdbc:postgresql://localhost:5432/StudentsDB");
            driverManagerDataSource.setUsername("postgres");
            driverManagerDataSource.setPassword("postgres");
            return driverManagerDataSource;
      }
      
      @Bean
      public ViewResolver viewResolver() {
            InternalResourceViewResolver resolver =
                 new InternalResourceViewResolver();
            resolver.setPrefix("/WEB-INF/views/");
            resolver.setSuffix(".jsp");
            resolver.setExposeContextBeansAsAttributes(true);
            return resolver;
      }
      @Override
      public void configureDefaultServletHandling(
           DefaultServletHandlerConfigurer configurer) {
            configurer.enable();
      }
}
