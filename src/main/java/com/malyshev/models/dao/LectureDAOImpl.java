package com.malyshev.models.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import com.malyshev.models.connector.Connector;
import com.malyshev.models.dao.Interfaces.LectureDAO;
import com.malyshev.models.pojo.Lecture;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

/**
 * Created by A.Malyshev on 24.02.17.
 */
@Repository
public class LectureDAOImpl implements LectureDAO {
      
      private static final Logger LOGGER = Logger.getLogger( LectureDAOImpl.class );
      
      private static final String SQL_GET_ALL = "SELECT * FROM main.lecture";
      private static final String SQL_CLOSE_LECTURES = "SELECT * FROM main.lecture where date_time > clock_timestamp() and date_time <= (clock_timestamp() + interval '1 hour')";
      private static final String SQL_ADD_LECTURE = "INSERT INTO main.lecture(name, subject, text, group_id, date_time ) VALUES (?, ?, ?, ?, ?);";
      private static final String SQL_DELETE_LECTURE = "DELETE FROM main.lecture WHERE id = ?";
      private static final String SQL_UPDATE_LECTURE = "UPDATE main.lecture SET name=?, subject=?, text=?, group_id=?, date_time=? WHERE id = ?; ";
      
      public List< Lecture > getAll( ) {
            List< Lecture > list = new ArrayList<>( );
            
            try {
                  Connector.initConnection( );
                  PreparedStatement preparedStatement = Connector.getConnection( )
                       .prepareStatement( SQL_GET_ALL );
                  ResultSet resultSet = preparedStatement.executeQuery( );
                  
                  list = getListFromResultSet(resultSet);
                  
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            } catch ( ClassNotFoundException e ) {
                  LOGGER.error( e );
            }
            
            return list;
      }
      
      public List< Lecture > getCloseLectures( ) {
            List< Lecture > list = new ArrayList<>( );
            
            try {
                  Connector.initConnection( );
                  PreparedStatement preparedStatement = Connector.getConnection( )
                       .prepareStatement( SQL_CLOSE_LECTURES );
                  ResultSet resultSet = preparedStatement.executeQuery( );
      
                  list = getListFromResultSet(resultSet);
                  
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            } catch ( ClassNotFoundException e ) {
                  LOGGER.error( e );
            }
            
            return list;
      }
      
      public void addLecture(Lecture lecture) {
            PreparedStatement preparedStatement = null;
            try {
                  Connector.initConnection( );
                  preparedStatement = Connector.getConnection( )
                       .prepareStatement( SQL_ADD_LECTURE );
                  preparedStatement.setString( 1, lecture.getName( ) );
                  preparedStatement.setString( 2, lecture.getSubject( ) );
                  preparedStatement.setString( 3, lecture.getText( ) );
                  preparedStatement.setInt( 4, lecture.getGroupId( ) );
                  preparedStatement.setTimestamp( 5,  new Timestamp( lecture.getDateTime( ).getTime().getTime()));
                  preparedStatement.executeUpdate( );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            } catch ( ClassNotFoundException e ) {
                  LOGGER.error( e );
            }
            LOGGER.info(
                 "Lecture with name = " + lecture.getName( )+ " was created" );
      }
      
      public void deleteLecture( Lecture lecture ) {
            PreparedStatement preparedStatement = null;
            try {
                  Connector.initConnection( );
                  preparedStatement = Connector.getConnection( )
                       .prepareStatement( SQL_DELETE_LECTURE );
                  preparedStatement.setInt( 1, lecture.getId( ) );
                  preparedStatement.executeUpdate( );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            } catch ( ClassNotFoundException e ) {
                  LOGGER.error( e );
            }
            LOGGER.info(
                 "Lecture with id = " + lecture.getId( ) + " and name = " + lecture.getName( )
                      + " was deleted" );
      }
      
      
      public void updateLecture(Lecture lecture) {
            PreparedStatement preparedStatement = null;
            try {
                  Connector.initConnection( );
                  preparedStatement = Connector.getConnection( )
                       .prepareStatement( SQL_UPDATE_LECTURE );
                  preparedStatement.setString( 1, lecture.getName( ) );
                  preparedStatement.setString( 2, lecture.getSubject( ) );
                  preparedStatement.setString( 3, lecture.getText( ) );
                  preparedStatement.setInt( 4, lecture.getGroupId( ) );
                  preparedStatement.setDate( 5, new Date( lecture.getDateTime( ).getTimeInMillis() ));
                  preparedStatement.setInt( 6, lecture.getId( ) );
                  preparedStatement.executeUpdate( );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            } catch ( ClassNotFoundException e ) {
                  LOGGER.error( e );
            }
            LOGGER.info(
                 "Lecture with id = " + lecture.getId( ) + " was updated" );
      }
      
      
      private List<Lecture> getListFromResultSet(ResultSet resultSet) throws SQLException {
            List<Lecture> list = new ArrayList<>(  );
            
            while ( resultSet.next( ) ) {
                  Lecture lecture = new Lecture( );
                  lecture.setId( resultSet.getInt( "id" ) );
                  lecture.setName( resultSet.getString( "name" ) );
                  lecture.setSubject( resultSet.getString( "subject" ) );
                  lecture.setText( resultSet.getString( "text" ) );
                  lecture.setGroupId( resultSet.getInt( "group_id" ) );
                  Calendar cal = Calendar.getInstance( );
                  cal.setTime( resultSet.getTimestamp( "date_time" ) );
                  lecture.setDateTime( cal);
                  list.add( lecture );
            }
            return list;
      }
}
