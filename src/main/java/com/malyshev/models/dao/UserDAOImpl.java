package com.malyshev.models.dao;

import com.malyshev.common.exceptions.UserDAOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.malyshev.models.connector.Connector;
import com.malyshev.models.dao.Interfaces.UserDAO;
import com.malyshev.models.pojo.User;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

/**
 * Created by A.Malyshev on 23.02.17.
 */
@Repository
public class UserDAOImpl implements UserDAO {
      
      private  static  final Logger LOGGER = Logger.getLogger( UserDAOImpl.class );
      
      private static final String SQL_FIND_USER_BY_LOGIN_AND_PASS = "SELECT * FROM main.user where login = ? and password = ?";
      private static final String SQL_FIND_USER_BY_LOGIN= "SELECT * FROM main.user where login = ?";
      private static final String SQL_CREATE_USER = "INSERT INTO  main.user(login, password, role) VALUES(?, ?, 'admin')";
      
      public User getUserByLoginAndPassword(String login, String password)
           throws UserDAOException {
            User user = new User();
            
            try  {
                  Connector.initConnection();
                  PreparedStatement preparedStatement = Connector.getConnection().prepareStatement(
                       SQL_FIND_USER_BY_LOGIN_AND_PASS );
                  preparedStatement.setString( 1, login );
                  preparedStatement.setString( 2, password );
      
                  ResultSet resultSet = preparedStatement.executeQuery();
                  
                  while ( resultSet.next() ) {
                        user.setIdUser( resultSet.getInt( "iduser" ) );
                        user.setUsername( resultSet.getString( "login" ) );
                        user.setPassword( resultSet.getString( "password" ) );
                        user.setRole( resultSet.getString( "role" ) );
                  }
                  
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            } catch ( ClassNotFoundException e ) {
                  LOGGER.error( e );
                  throw new UserDAOException();
            }
      
            return user;
      }
      
      public User getUserByLogin(String login)
           throws UserDAOException {
            User user = new User( );
      
            try {
                  Connector.initConnection( );
                  PreparedStatement preparedStatement = Connector.getConnection( ).prepareStatement(
                       SQL_FIND_USER_BY_LOGIN );
                  preparedStatement.setString( 1, login );
            
                  ResultSet resultSet = preparedStatement.executeQuery( );
            
                  while ( resultSet.next( ) ) {
                        user.setIdUser( resultSet.getInt( "iduser" ) );
                        user.setUsername( resultSet.getString( "login" ) );
                        user.setPassword( resultSet.getString( "password" ) );
                        user.setRole( resultSet.getString( "role" ) );
                  }
            
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            } catch ( ClassNotFoundException e ) {
                  LOGGER.error( e );
                  throw new UserDAOException( );
            }
      
            return user;
      }
      
      
      public boolean registrateUser(String login, String password) throws UserDAOException {
      
            try  {
                  Connector.initConnection();
                  PreparedStatement preparedStatement = Connector.getConnection().prepareStatement( SQL_CREATE_USER );
//                  preparedStatement.setInt( 1, 2 );
                  preparedStatement.setString( 1, login );
                  preparedStatement.setString( 2, password );
                  
                  return preparedStatement.executeUpdate() > 0;
            
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            } catch ( ClassNotFoundException e ) {
                  LOGGER.error( e );
                  throw new UserDAOException();
            }
            return false;
      }
}
