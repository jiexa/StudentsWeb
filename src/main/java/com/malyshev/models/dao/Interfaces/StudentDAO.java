package com.malyshev.models.dao.Interfaces;

import java.util.List;
import com.malyshev.models.pojo.Student;

/**
 * Created by A.Malyshev on 02.03.17.
 */
public interface StudentDAO {
      
      List< Student > getAll( );
      
      void deleteStudent(Student student);
      
      void addStudent(Student student);
      
      void updateStudent(Student student);
}
