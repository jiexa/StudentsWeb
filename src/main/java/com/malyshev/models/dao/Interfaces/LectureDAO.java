package com.malyshev.models.dao.Interfaces;

import java.util.List;
import com.malyshev.models.pojo.Lecture;

/**
 * Created by A.Malyshev on 02.03.17.
 */
public interface LectureDAO {
      List< Lecture > getAll( );
      List< Lecture > getCloseLectures( );
      void addLecture(Lecture lecture);
      void deleteLecture( Lecture lecture );
      void updateLecture(Lecture lecture);
}
