package com.malyshev.models.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.malyshev.models.connector.Connector;
import com.malyshev.models.dao.Interfaces.StudentDAO;
import com.malyshev.models.pojo.Student;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

/**
 * Created by A.Malyshev on 23.02.17.
 */
@Repository
public class StudentDAOImpl implements StudentDAO {
      
      private static final Logger LOGGER = Logger.getLogger( StudentDAOImpl.class );
      
      private static final String SQL_GET_ALL = "SELECT * FROM main.student";
      private static final String SQL_ADD_STUDENT = "INSERT INTO main.student(name, birthdate, ismale, group_id) VALUES (?, ?, ?, ?);";
      private static final String SQL_DELETE_STUDENT = "DELETE FROM main.student WHERE id = ?";
      private static final String SQL_UPDATE_STUDENT = "UPDATE main.student SET name=?, birthdate=?, group_id=? WHERE id = ?; ";
      
      public List< Student > getAll( ) {
            System.out.println("------------- Students are listed ---------" );
            List< Student > list = new ArrayList<>( );
            
            try {
                  Connector.initConnection( );
                  PreparedStatement preparedStatement = Connector.getConnection( )
                       .prepareStatement( SQL_GET_ALL );
                  ResultSet resultSet = preparedStatement.executeQuery( );
                  
                  while ( resultSet.next( ) ) {
                        Student student = new Student( );
                        student.setId( resultSet.getInt( "id" ) );
                        student.setName( resultSet.getString( "name" ) );
                        student.setBirthDate( resultSet.getDate( "birthdate" ) );
                        student.setMale( resultSet.getBoolean( "ismale" ) );
                        student.setGroupId( resultSet.getInt( "group_id" ) );
                        list.add( student );
                  }
                  
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            } catch ( ClassNotFoundException e ) {
                  LOGGER.error( e );
            }
            
            return list;
      }
      
      public void addStudent(Student student) {
            PreparedStatement preparedStatement = null;
            try {
                  Connector.initConnection( );
                  preparedStatement = Connector.getConnection( )
                       .prepareStatement( SQL_ADD_STUDENT );
                  preparedStatement.setString( 1, student.getName( ) );
                  preparedStatement.setDate( 2, student.getBirthDate( ) );
                  preparedStatement.setBoolean( 3, student.isMale( ) );
                  preparedStatement.setInt( 4, student.getGroupId( ) );
                  preparedStatement.executeUpdate( );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            } catch ( ClassNotFoundException e ) {
                  LOGGER.error( e );
            }
            LOGGER.info(
                 "Student with name = " + student.getName( )+ " was created" );
      }
      
      public void deleteStudent( Student student ) {
            PreparedStatement preparedStatement = null;
            try {
                  Connector.initConnection( );
                  preparedStatement = Connector.getConnection( )
                       .prepareStatement( SQL_DELETE_STUDENT );
                  preparedStatement.setInt( 1, student.getId( ) );
                  preparedStatement.executeUpdate( );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            } catch ( ClassNotFoundException e ) {
                  LOGGER.error( e );
            }
            LOGGER.info(
                 "Student with id = " + student.getId( ) + " and name = " + student.getName( )
                      + " was deleted" );
      }
      
      public void updateStudent(Student student) {
            PreparedStatement preparedStatement = null;
            try {
                  Connector.initConnection( );
                  preparedStatement = Connector.getConnection( )
                       .prepareStatement( SQL_UPDATE_STUDENT );
                  preparedStatement.setString(1,  student.getName() );
                  preparedStatement.setDate(2,  student.getBirthDate() );
                  preparedStatement.setInt(3,  student.getGroupId() );
                  preparedStatement.setInt( 4, student.getId( ) );
                  preparedStatement.executeUpdate( );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            } catch ( ClassNotFoundException e ) {
                  LOGGER.error( e );
            }
            LOGGER.info(
                 "Student with id = " + student.getId( ) + " was updated" );
      }
}
