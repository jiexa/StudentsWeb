package com.malyshev.models.connector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by A.Malyshev on 23.02.17.
 */
public class Connector {
      private String user = "root";//Логин пользователя
      private static String url = "jdbc:postgresql://localhost:5432/StudentsDB";
      private static String login = "postgres";
      private static String password = "postgres";
      private static Connection conn;
      
      
      private Connector() {
      }
      
      /**
       * Инициализировать подключение
       * @throws SQLException
       */
      public static synchronized void initConnection() throws SQLException, ClassNotFoundException {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(url, login, password);
      }
      
      /**
       * Получить подключение
       * @return Подключение к БД
       */
      public static synchronized Connection getConnection() {
            return conn;
      }
}
