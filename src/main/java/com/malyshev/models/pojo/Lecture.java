package com.malyshev.models.pojo;

import java.util.Calendar;

/**
 * Created by A.Malyshev on 24.02.17.
 */
public class Lecture {
      private Integer id;
      private String name;
      private String subject;
      private String text;
      private Integer groupId;
      private Calendar dateTime;
      
      public String getSubject( ) {
            return subject;
      }
      
      public void setSubject( String subject ) {
            this.subject = subject;
      }
      
      public Integer getGroupId( ) {
            return groupId;
      }
      
      public void setGroupId( Integer groupId ) {
            this.groupId = groupId;
      }
      
      public Calendar getDateTime( ) {
            return dateTime;
      }
      
      public void setDateTime( Calendar dateTime ) {
            this.dateTime = dateTime;
      }
      
      public Integer getId( ) {
            return id;
      }
      
      public void setId( Integer id ) {
            this.id = id;
      }
      
      public String getName( ) {
            return name;
      }
      
      public void setName( String name ) {
            this.name = name;
      }
      
      public String getText( ) {
            return text;
      }
      
      public void setText( String text ) {
            this.text = text;
      }
}
