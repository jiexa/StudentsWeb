package com.malyshev.models.pojo;

import java.sql.Date;

/**
 * Created by A.Malyshev on 23.02.17.
 */
public class Student {
      private Integer id;
      private String name;
      private Date birthDate;
      private boolean isMale;
      private Integer groupId;
      
      public Student( ) {
      }
      
      public Student( Integer id, String name, Date birthDate, boolean isMale, Integer groupId ) {
            this.id = id;
            this.name = name;
            this.birthDate = birthDate;
            this.isMale = isMale;
            this.groupId = groupId;
      }
      
      public Integer getId( ) {
            return id;
      }
      
      public void setId( Integer id ) {
            this.id = id;
      }
      
      public String getName( ) {
            return name;
      }
      
      public void setName( String name ) {
            this.name = name;
      }
      
      public Date getBirthDate( ) {
            return birthDate;
      }
      
      public void setBirthDate( Date birthDate ) {
            this.birthDate = birthDate;
      }
      
      public boolean isMale( ) {
            return isMale;
      }
      
      public void setMale( boolean male ) {
            isMale = male;
      }
      
      public Integer getGroupId( ) {
            return groupId;
      }
      
      public void setGroupId( Integer groupId ) {
            this.groupId = groupId;
      }
}
