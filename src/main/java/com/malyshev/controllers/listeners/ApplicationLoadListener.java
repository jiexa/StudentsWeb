package com.malyshev.controllers.listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;

/**
 * Created by A.Malyshev on 24.02.17.
 */
public class ApplicationLoadListener implements ServletContextListener {
      
      private static final Logger LOGGER = Logger.getLogger( ApplicationLoadListener.class );
      
      @Override
      public void contextInitialized( ServletContextEvent sce ) {
            LOGGER.trace( "Web-site started" );
      }
      
      @Override
      public void contextDestroyed( ServletContextEvent sce ) {
            LOGGER.trace( "Web-site stopped" );
      }
}
