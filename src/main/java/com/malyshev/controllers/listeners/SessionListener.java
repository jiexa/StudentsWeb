package com.malyshev.controllers.listeners;

import com.malyshev.common.notifiers.Mailer;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import com.malyshev.models.pojo.Lecture;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.malyshev.services.interfaces.LectureService;

/**
 * Created by A.Malyshev on 24.02.17.
 */
public class SessionListener implements HttpSessionListener, HttpSessionAttributeListener,
     ServletContextListener {
      
      private LectureService lectureService;
      
      @Autowired
      public void setLectureService( LectureService lectureService ) {
            this.lectureService = lectureService;
      }
      
      
      private static final Logger LOGGER = Logger.getLogger( SessionListener.class );
      
      @Override
      public void contextInitialized(ServletContextEvent event) {
      
            WebApplicationContextUtils
                 .getRequiredWebApplicationContext(event.getServletContext())
                 .getAutowireCapableBeanFactory()
                 .autowireBean(this);
      }
      
      @Override
      public void contextDestroyed( ServletContextEvent sce ) {
            
      }
      
      @Override
      public void sessionCreated( HttpSessionEvent se ) {
            LOGGER.trace( "Session created");
            Thread thread = new Thread( new Runnable( ) {
                  @Override
                  public void run( ) {
                        while ( true ) {
                              try {
                                    Thread.sleep( 1000 * 60 * 60);
                                    LOGGER.info( "1 minute is passed" );
                                    StringBuilder sb = new StringBuilder( "lectures within next hour: \n" );
                                    for( Lecture lecture : lectureService.getCloseLectureList() ){
                                          sb.append( lecture.getName() + "\n");
                                    }
                                    Mailer.sendMail( "malyshevalexey8@gmail.com", "Text", sb.toString() );
                                    LOGGER.trace( "A letter was sent" );
                              } catch ( InterruptedException e ) {
                                    LOGGER.error( e );
                              }
                        }
                  }
            } );
            thread.start();
      }
      
      @Override
      public void sessionDestroyed( HttpSessionEvent se ) {
            LOGGER.trace( "Session destroyed" );
      
      }
      
      @Override
      public void attributeAdded( HttpSessionBindingEvent event ) {
//            Mailer.sendMail( "malyshevalexey8@gmail.com", "Text", "text message " );
            LOGGER.trace( "added new attribute" );
      }
      
      @Override
      public void attributeRemoved( HttpSessionBindingEvent event ) {
            LOGGER.info( "Attribute removed" + event.getValue() + " " + event.getSession().getAttributeNames().nextElement() );
      }
      
      @Override
      public void attributeReplaced( HttpSessionBindingEvent event ) {
            LOGGER.info( "Attribute replaced" + event.getValue() + " " + event.getSession().getAttributeNames().nextElement() );
      }
}
