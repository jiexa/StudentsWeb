package com.malyshev.controllers;

import com.malyshev.models.pojo.Student;
import com.malyshev.services.StudentServiceImpl;
import com.malyshev.services.interfaces.StudentService;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by A.Malyshev on 06.03.17.
 */
@Controller
@RequestMapping(value = "list")
public class ListController {
      
      private static final Logger LOGGER = Logger.getLogger( ListController.class );
      
      private StudentService studentService;
      
      @Autowired
      public void setStudentService( StudentService studentService ) {
            this.studentService = studentService;
      }
      
      @RequestMapping( method = RequestMethod.GET)
      public String showList (Model model, HttpServletRequest req ) {
            LOGGER.info( "getting list" );
            List<Student> students;
            students = studentService.getStudentList();
            model.addAttribute( "students", students );
            req.getSession().setAttribute( "studentList", students );
      
            Map<String, Integer> actions = null;
            actions = StudentServiceImpl.getActionsList();
            model.addAttribute( "actions", actions );
            
            return "list";
      }
      @RequestMapping(  method = RequestMethod.POST )
      public String editList(@RequestParam(name = "action") String action,
                                                        @RequestParam(name = "selectedStudent") String selectedStudent,
                                                            HttpServletRequest req) {
            
            LOGGER.info( "param is " + action );
            Integer actionId = new Integer( action );
      
            if (actionId == 2) {
                  
                  studentService.deleteStudent( selectStudent( selectedStudent, req.getSession().getAttribute( "studentList" )) );
                  req.getSession().removeAttribute( "studentList" );
                  return "redirect:list";
            } else if (actionId == 1) {
                  req.getSession().setAttribute( "selectedStudent", selectStudent( selectedStudent, req.getSession().getAttribute( "studentList" ) ));
                  return "redirect:edit";
            }
      
            return "redirect:list";
      }
      
      private Student selectStudent(String selectedStudent, Object studentList ) {
            Student stud = null;
            List<Student> students = (List<Student> )studentList;
            for( Student student : students ) {
                  if (student.toString().equals( selectedStudent ))
                        stud = student;
            }
            return stud;
      }
}
