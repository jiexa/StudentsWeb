package com.malyshev.controllers;

import com.malyshev.common.Utils;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.malyshev.models.pojo.Lecture;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import com.malyshev.services.LectureServiceImpl;
import com.malyshev.services.interfaces.LectureService;

/**
 * Created by A.Malyshev on 24.02.17.
 */
public class LectureAddServlet extends HttpServlet {
      
      private LectureService lectureService;
      
      @Autowired
      public void setLectureService( LectureService lectureService ) {
            this.lectureService = lectureService;
      }
      
      private static final Logger LOGGER = Logger.getLogger( LectureAddServlet.class );
      
      @Override
      public void init(ServletConfig config) throws ServletException {
            super.init( config );
            SpringBeanAutowiringSupport
                 .processInjectionBasedOnServletContext( this, config.getServletContext( ) );
      }
      
      @Override
      protected void doGet( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            
            LOGGER.info( "doing GET" );
            
            req.getRequestDispatcher( "/include/lectures/lectureAdd.jsp" ).forward( req,resp );
      }
      
      @Override
      protected void doPost( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            
            Lecture newLecture = new Lecture(  );
            newLecture.setName(req.getParameter( "addName" ));
            newLecture.setSubject(req.getParameter( "addSubject" ));
            newLecture.setText(req.getParameter( "addText" ));
            newLecture.setGroupId(Integer.parseInt( req.getParameter( "addGroup" )));
            String dateTime = req.getParameter( "addDateTime" );
            newLecture.setDateTime(Utils.stringToCalendar( dateTime));
            lectureService.addLecture( newLecture );
            
            LOGGER.info( "New student was added (name = " + newLecture.getName() +")" );
            
            resp.sendRedirect( "/Students/lecture_list" );
      }
}
