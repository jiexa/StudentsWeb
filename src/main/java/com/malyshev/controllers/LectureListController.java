package com.malyshev.controllers;

import com.malyshev.models.pojo.Lecture;
import com.malyshev.models.pojo.Student;
import com.malyshev.services.LectureServiceImpl;
import com.malyshev.services.StudentServiceImpl;
import com.malyshev.services.interfaces.LectureService;
import com.malyshev.services.interfaces.StudentService;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by A.Malyshev on 06.03.17.
 */
@Controller
@RequestMapping(value = "lectureList")
public class LectureListController {
      
      private static final Logger LOGGER = Logger.getLogger( LectureListController.class );
      
      private LectureService lectureService;
      
      @Autowired
      public void setLectureService( LectureService lectureService ) {
            this.lectureService = lectureService;
      }
      
      @RequestMapping( method = RequestMethod.GET)
      public String showList (Model model, HttpServletRequest req ) {
            List<Lecture > lectures = lectureService.getLectureList();
            req.getSession().setAttribute( "lectures", lectures  );
            model.addAttribute( "lectures", lectures );
      
            Map<String, Integer> actions = null;
            actions = LectureServiceImpl.getActionsList();
            model.addAttribute( "actions", actions );
            
            return "lectures/lectureList";
      }
      @RequestMapping(  method = RequestMethod.POST )
      public String editList(@RequestParam(name = "action") String action,
                                                        @RequestParam(name = "selectedStudent") String selectedLecture,
                                                            HttpServletRequest req) {
            
            String param  = req.getParameter( "action" );
            LOGGER.info( "param is " + param );
            Integer actionId = new Integer( param );
      
            if (actionId == 2) {
                  lectureService.deleteLecture( selectLecture( selectedLecture, req.getSession().getAttribute( "lectures" ) ) );
                  req.getSession().removeAttribute( "lectures" );
                 return "redirect:lectureList";
            } else if (actionId == 1) {
                  req.getSession().setAttribute( "selectedLecture", selectLecture( selectedLecture, req.getSession().getAttribute( "lectures" ) ) );
                  return "redirect:lectureEdit";
            }
      
            return "redirect:list";
      }
      
      private Lecture selectLecture(String selectedLecture, Object lectures ) {
            Lecture lect= null;
            List<Lecture> lectureList = (List<Lecture> ) lectures;
            for( Lecture lecture : lectureList ) {
                  if (lecture.toString().equals( selectedLecture ));
                        lect = lecture;
            }
            return lect;
      }
}
