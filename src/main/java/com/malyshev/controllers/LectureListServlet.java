package com.malyshev.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.malyshev.models.pojo.Lecture;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import com.malyshev.services.LectureServiceImpl;
import com.malyshev.services.interfaces.LectureService;

/**
 * Created by A.Malyshev on 24.02.17.
 */
public class LectureListServlet extends HttpServlet {
      
      private LectureService lectureService;
      
      @Autowired
      public void setLectureService( LectureService lectureService ) {
            this.lectureService = lectureService;
      }
      
      private static final Logger LOGGER = Logger.getLogger( LectureListServlet.class );
      
      @Override
      public void init(ServletConfig config) throws ServletException {
            super.init( config );
            SpringBeanAutowiringSupport
                 .processInjectionBasedOnServletContext( this, config.getServletContext( ) );
      }
      
      @Override
      protected void doGet( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            LOGGER.info( "doing GET" );
            
            List<Lecture > lectures = lectureService.getLectureList();
            req.getSession().setAttribute( "lectures", lectures  );
            req.setAttribute( "lectures", lectures );
            
            Map<String, Integer> actions = null;
            actions = LectureServiceImpl.getActionsList();
            req.setAttribute( "actions", actions );
            
            req.getRequestDispatcher( "/include/lectures/lectureList.jsp" ).forward( req, resp );
      }
      
      @Override
      protected void doPost( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
      
            String param  = req.getParameter( "action" );
            LOGGER.info( "doing POST" );
            LOGGER.info( "param is " + param );
            Integer actionId = new Integer( param );
      
            if (actionId == 2) {
                  lectureService.deleteLecture( selectLecture( req ) );
                  req.getSession().removeAttribute( "lectures" );
                  resp.sendRedirect( "/Students/lecture_list" );
            } else if (actionId == 1) {
                  req.getSession().setAttribute( "selectedLecture", selectLecture( req ) );
                  resp.sendRedirect( "/Students/lecture_edit" );
            }
      }
      
      private Lecture selectLecture(HttpServletRequest req) {
            Lecture lect= null;
            String selectedLecture= req.getParameter( "selectedLecture" );
            List<Lecture> lectures = (List<Lecture> ) req.getSession( ).getAttribute( "lectures" );
            for( Lecture lecture : lectures ) {
                  if (lecture.toString().equals( selectedLecture ))
                        lect = lecture;
            }
            return lect;
      }
}
