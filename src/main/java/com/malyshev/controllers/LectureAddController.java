package com.malyshev.controllers;

import com.malyshev.common.Utils;
import com.malyshev.models.pojo.Lecture;
import com.malyshev.models.pojo.Student;
import com.malyshev.services.interfaces.LectureService;
import com.malyshev.services.interfaces.StudentService;
import java.util.Calendar;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by A.Malyshev on 06.03.17.
 */
@Controller
@RequestMapping( value = "lectureAdd")
public class LectureAddController {
      
      private static final Logger LOGGER = Logger.getLogger( LectureAddController.class );
      
      private LectureService lectureService;
      
      @Autowired
      public void setLectureService( LectureService lectureService ) {
            this.lectureService = lectureService;
      }
      
      @RequestMapping( method = RequestMethod.GET )
      public String showAddLecturePage( ) {
            
            return "lectures/lectureAdd";
      }
      
      @RequestMapping( method = RequestMethod.POST )
      public String addLecture(Lecture lecture) {
            
            lecture.setDateTime( Calendar.getInstance());
            
            lectureService.addLecture( lecture );
      
            LOGGER.info( "New lecture was added (name = " + lecture.getName() +")" );
           
            return "redirect:lectureList";
      }
}
