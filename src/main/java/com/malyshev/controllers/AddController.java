package com.malyshev.controllers;

import com.malyshev.common.Utils;
import com.malyshev.common.exceptions.UserDAOException;
import com.malyshev.models.pojo.Student;
import com.malyshev.models.pojo.User;
import com.malyshev.services.interfaces.StudentService;
import com.malyshev.services.interfaces.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by A.Malyshev on 06.03.17.
 */
@Controller
@RequestMapping( value = "add")
public class AddController {
      
      private static final Logger LOGGER = Logger.getLogger( AddController.class );
      
      private StudentService studentService;
      
      @Autowired
      public void setStudentService( StudentService studentService ) {
            this.studentService = studentService;
      }
      
      @RequestMapping( method = RequestMethod.GET )
      public String showAddPage( ) {
            
            return "add";
      }
      
      @RequestMapping( method = RequestMethod.POST )
      public String addStudent(Student student) {
      
            studentService.addStudent( student );
      
            LOGGER.info( "New student was added (name = " + student.getName() +")" );
      
            return "redirect:list";
      }
}
