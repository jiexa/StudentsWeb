package com.malyshev.controllers;

import com.malyshev.common.exceptions.UserDAOException;
import com.malyshev.models.pojo.User;
import com.malyshev.services.interfaces.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by A.Malyshev on 06.03.17.
 */
@Controller
@RequestMapping( value = "registration")
public class RegistrationController {
      
      private static final Logger LOGGER = Logger.getLogger( RegistrationController.class );
      
      private UserService userService;
      
      @Autowired
      public void setUserService( UserService userService ) {
            this.userService = userService;
      }
      
      @RequestMapping(method = RequestMethod.GET )
      public String showRegPage( ) {
            
            return "registration";
      }
      
      @RequestMapping(method = RequestMethod.POST )
      public String tryRegister(User user) {
      
            try {
                  if ( userService.registrationUser( user.getUsername(), user.getPassword() )) {
                        LOGGER.trace( "Authorized" );
                        return "redirect:list" ;
                  } else {
                        LOGGER.trace( "Unauthorized" );
                        return "redirect:error";
                  }
            } catch ( UserDAOException e ) {
                  LOGGER.error( e );
                  return "redirect:error";
            }
            
      }
}
