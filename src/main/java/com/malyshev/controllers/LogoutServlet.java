package com.malyshev.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 * Created by A.Malyshev on 24.02.17.
 */
public class LogoutServlet extends HttpServlet {
      private static final long serialVersionUID = 1L;
      
      private static final Logger LOGGER = Logger.getLogger( LogoutServlet.class );
      
      @Override
      protected void doGet( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            
            HttpSession session = req.getSession(false);
            
            if (session != null) {
                  session.invalidate( );
                  LOGGER.info( "Session was invalidated");
            }
            
            resp.sendRedirect( "/Students/login" );
      }
      
      protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            response.setContentType("text/jsp");
            Cookie[] cookies = request.getCookies();
            if(cookies != null){
                  for(Cookie cookie : cookies){
                        if(cookie.getName().equals("JSESSIONID")){
                              System.out.println("JSESSIONID="+cookie.getValue());
                              break;
                        }
                  }
            }
            //invalidate the session if exists
            HttpSession session = request.getSession(false);
            System.out.println("User="+session.getAttribute("user"));
            if(session != null){
                  session.invalidate();
            }
            response.sendRedirect("/Students/login");
      }

}
