package com.malyshev.controllers;

import com.malyshev.common.exceptions.UserDAOException;
import com.malyshev.models.pojo.User;
import com.malyshev.services.interfaces.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by A.Malyshev on 06.03.17.
 */
@Controller
@RequestMapping( value = "login")
public class LoginController {
      
      private static final Logger LOGGER = Logger.getLogger( LoginController.class );
      
      private UserService userService;
      
      @Autowired
      public void setUserService( UserService userService ) {
            this.userService = userService;
      }
      
      
      @RequestMapping(method = RequestMethod.GET )
      public String showLoginPage( ) {
            LOGGER.info( "doing GET" );
            return "login";
      }
      
      @RequestMapping(method = RequestMethod.POST )
      public String tryLogIn(User user) {
            
            try {
                  if (userService.authorize( user.getUsername(), user.getPassword() )) {
                        LOGGER.info( "Authorized" );
                        return "redirect:list";
                  } else {
                        LOGGER.info( "Unauthorized" );
                        return "redirect:login";
                  }
            } catch ( UserDAOException e ) {
                  LOGGER.error(e);
                  return "redirect:error";
            }
            
      }
}
