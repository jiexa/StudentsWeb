package com.malyshev.controllers;

import com.malyshev.models.pojo.Student;
import com.malyshev.services.interfaces.StudentService;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by A.Malyshev on 06.03.17.
 */
@Controller
@RequestMapping( value = "edit")
public class EditController {
      
      private static final Logger LOGGER = Logger.getLogger( EditController.class );
      
      private StudentService studentService;
      
      @Autowired
      public void setStudentService( StudentService studentService ) {
            this.studentService = studentService;
      }
      
      @RequestMapping( method = RequestMethod.GET )
      public String showEditPage( HttpServletRequest req) {
      
      
            req.setAttribute( "editingStudent",
                 req.getSession( ).getAttribute( "selectedStudent" ) );
            
            return "edit";
      }
      
      @RequestMapping( method = RequestMethod.POST )
      public String addStudent(Student student, HttpServletRequest req) {
      
            Student selectedStudent = ( Student ) req.getSession( )
                 .getAttribute( "selectedStudent" );
            Student editedStudent = new Student( );
            editedStudent.setId( selectedStudent.getId( ) );
            editedStudent.setName( student.getName());
            editedStudent.setBirthDate( student.getBirthDate() );
            editedStudent.setMale( selectedStudent.isMale( ) );
            editedStudent.setGroupId( selectedStudent.getGroupId( ) );
      
            studentService.updateStudent( editedStudent );
      
            LOGGER.info( "Input date processed" );
            
            return "redirect:list";
      }
}
