package com.malyshev.controllers;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.malyshev.models.pojo.Lecture;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import com.malyshev.services.LectureServiceImpl;
import com.malyshev.services.interfaces.LectureService;

/**
 * Created by A.Malyshev on 24.02.17.
 */
public class LectureEditServlet extends HttpServlet {
      
      private LectureService lectureService;
      
      @Autowired
      public void setLectureService( LectureService lectureService ) {
            this.lectureService = lectureService;
      }
      
      private static final Logger LOGGER = Logger.getLogger( LectureEditServlet.class );
      
      @Override
      public void init(ServletConfig config) throws ServletException {
            super.init( config );
            SpringBeanAutowiringSupport
                 .processInjectionBasedOnServletContext( this, config.getServletContext( ) );
      }
      
      @Override
      protected void doGet( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            
            LOGGER.info( "doing GET" );
            
            req.setAttribute( "editingLecture",
                 req.getSession( ).getAttribute( "selectedLecture" ) );
            
            req.getRequestDispatcher( "/include/lectures/lectureEdit.jsp" ).forward( req, resp );
      }
      
      @Override
      protected void doPost( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            
            Lecture selectedLecture = ( Lecture ) req.getSession( )
                 .getAttribute( "selectedLecture" );
            Lecture editedLecture = new Lecture( );
            editedLecture.setId( selectedLecture.getId( ) );
            editedLecture.setName( req.getParameter( "editName" ) );
            editedLecture.setText( req.getParameter( "editText" ) );
      
            lectureService.updateLecture( editedLecture );
            
            LOGGER.info( "Input date processed" );
            
            resp.sendRedirect( "/Students/lecture_list" );
      }
}
