package com.malyshev.services;

import com.malyshev.common.exceptions.UserDAOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import com.malyshev.models.dao.UserDAOImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.malyshev.services.interfaces.UserService;

/**
 * Created by A.Malyshev on 23.02.17.
 */
@Service("userService")
public class UserServiceImpl implements UserService, UserDetailsService {
      
      private  static  final Logger LOGGER = Logger.getLogger( UserServiceImpl.class );
      
      private UserDAOImpl userDAOImpl;
      
      @Autowired
      public void setUserDAO( UserDAOImpl userDAOImpl ) {
            this.userDAOImpl = userDAOImpl;
      }
      
      private int anInt = 0;
      
      public boolean authorize( String login, String password ) throws UserDAOException {
      
            Random random = new Random( );
            if (anInt == 0 ) {
                  anInt = random.nextInt( );
            }
            
            LOGGER.info( "Userservice int = " + anInt );
            
            if ( userDAOImpl.getUserByLoginAndPassword( login, password ).getIdUser( ) != null ) {
                  return true;
            } else {
                  return false;
            }
      }
      
      public boolean registrationUser(String login, String password)
           throws UserDAOException {
            
            return userDAOImpl.registrateUser( login, password );
      }
      
      @Override
      public UserDetails loadUserByUsername( String username ) throws UsernameNotFoundException {
            com.malyshev.models.pojo.User user = null;
            
            try {
                  user =  userDAOImpl.getUserByLogin( username );
            } catch ( UserDAOException e ) {
                  LOGGER.equals( e );
            }
            List<GrantedAuthority> authorities = new ArrayList<>(  );
            authorities.add( new SimpleGrantedAuthority( user.getRole() ) );
            return new User(user.getUsername(), user.getPassword(), authorities);
      }
}
