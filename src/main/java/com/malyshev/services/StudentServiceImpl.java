package com.malyshev.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.malyshev.models.dao.Interfaces.StudentDAO;
import com.malyshev.models.pojo.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.malyshev.services.interfaces.StudentService;

/**
 * Created by A.Malyshev on 23.02.17.
 */
@Service("studentService")
public class StudentServiceImpl implements StudentService{
      
      private StudentDAO studentDAO;
      
      @Autowired
      public void setStudentDAO( StudentDAO studentDAO ) {
            this.studentDAO = studentDAO;
      }
      
      public List<Student> getStudentList() {
            
            return studentDAO.getAll(  );
      }
      
      public void deleteStudent(Student student) {
            studentDAO.deleteStudent( student );
      }
      
      public void addStudent(Student student) {
            studentDAO.addStudent( student );
      }
      
      
      public void updateStudent(Student student) {
            studentDAO.updateStudent(student);
      }
      
      public static Map<String, Integer> getActionsList() {
            Map<String, Integer> actions = new HashMap<>( 8 );
            actions.put( "Редактировать", 1 );
            actions.put( "Удалить", 2 );
            
            return actions;
      }
      
      
}
