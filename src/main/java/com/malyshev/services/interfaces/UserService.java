package com.malyshev.services.interfaces;

import com.malyshev.common.exceptions.UserDAOException;

/**
 * Created by A.Malyshev on 02.03.17.
 */
public interface UserService {
      
      boolean authorize( String login, String password ) throws UserDAOException;
      
      boolean registrationUser( String login, String password ) throws UserDAOException;
}
