package com.malyshev.services.interfaces;

import java.util.List;
import com.malyshev.models.pojo.Lecture;

/**
 * Created by A.Malyshev on 02.03.17.
 */
public interface LectureService {
      List< Lecture > getLectureList( );
      List< Lecture > getCloseLectureList( );
      void deleteLecture( Lecture lecture );
      void addLecture( Lecture lecture );
      void updateLecture(Lecture lecture);
}
