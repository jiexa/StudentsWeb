package com.malyshev.services.interfaces;

import java.util.List;
import com.malyshev.models.pojo.Student;

/**
 * Created by A.Malyshev on 02.03.17.
 */
public interface StudentService {
      List<Student> getStudentList();
      void deleteStudent(Student student);
      void addStudent(Student student);
      void updateStudent(Student student);
}
