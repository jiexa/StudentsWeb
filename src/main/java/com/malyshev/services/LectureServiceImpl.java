package com.malyshev.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.malyshev.models.dao.Interfaces.LectureDAO;
import com.malyshev.models.pojo.Lecture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.malyshev.services.interfaces.LectureService;

/**
 * Created by A.Malyshev on 23.02.17.
 */
@Service("lectureService")
public class LectureServiceImpl implements LectureService {
      
      private LectureDAO lectureDAO;
      
      @Autowired
      public void setLectureDAO( LectureDAO lectureDAO ) {
            this.lectureDAO = lectureDAO;
      }
      
      public List< Lecture > getLectureList( ) {
            
            return lectureDAO.getAll( );
      }
      
      public  List< Lecture > getCloseLectureList( ) {
            
            return lectureDAO.getCloseLectures( );
      }
      
      public  void deleteLecture( Lecture lecture ) {
            lectureDAO.deleteLecture( lecture );
      }
      
      public  void addLecture( Lecture lecture ) {
            lectureDAO.addLecture( lecture );
      }
      
      
      public void updateLecture( Lecture lecture ) {
            lectureDAO.updateLecture(lecture);
      }
      
      
      public static Map< String, Integer > getActionsList( ) {
            Map< String, Integer > actions = new HashMap<>( 8 );
            actions.put( "Редактировать", 1 );
            actions.put( "Удалить", 2 );
            
            return actions;
      }
      
}
